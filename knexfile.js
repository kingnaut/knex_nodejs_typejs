// Update with your config settings.

module.exports = {
database: {
  development: {
    client: 'mysql',
      connection: {
        database: 'test_1',
        user:'MANHTUAN',
        password: '',
        host: 'localhost',
        port: 3306
      },
      pool: {
        min: 2,
        max: 10
      },
      migrations: {
        tableName: 'migrations'
      },
      seeds: {
        directory: 'seeds'
      }
    },
}
};
