* một sô lệnh quen thuộc:
* sudo npm i @types/node **cài đặt type/node**
*  sử dụng máy chủ: ** máy chủ GraphQL (**npm install graphql-yoga**)**
* NPM quản lý các thư viện Javascript **sudo npm install**
* lệnh **tsc**:khởi chạy sever
* lệnh  **tsc src/index.ts** tạo dist
* lệnh **node index.js** để khởi chạy project





1. Chạy server
2. Tạo migrate thêm mới bảng persons với các trường sau:
    lệnh tạo bảng mới (knex)" ***knex migrate:make namegigrate***
    lệnh đưa migaret lên server **knex migrate:latest    //  đưa các seed lên mysql (*knex seed:run*) **
* id: integer, auto increment
* name: string
* age: string
* gender: string

3. Thêm seeding cho bảng persons (dữ liệu tuỳ ý)
lệnh: **seed:make <name>**
4. Thêm schema person với typeDefs(**Hằng số typeDefs định nghĩa lược đồ GraphQL của bạn. Ở đây, nó định nghĩa một Query type đơn giản với một field**)  
resolves(**GraphQL schema và các resolvers được đóng gói và chuyển tới GraphQLServer được import từ graphql-yoga. Điều này cho máy chủ biết các hoạt động API được chấp nhận và cách chúng được giải quyết.** (*tương tự member)    
5. Thêm model person thực hiện các chức năng sau:
* Query
   - getAllPersons: [Person]
   - findPerson(id): Person

* Mutation
   - createPerson: Person
   - updatePerson: Person
   - deletPerson: Person
