"use strict";
exports.__esModule = true;
var connectors_1 = require("../connectors");
var bookshelf = require('bookshelf')(connectors_1.knex);
var Admin = bookshelf.Model.extend({
    tableName: 'admins',
    role: function () {
        return this.belongsTo(Role);
    }
});
var Role = bookshelf.Model.extend({
    tableName: 'roles'
});
var AdminM = /** @class */ (function () {
    function AdminM() {
    }
    AdminM.prototype.getAllAdmins = function () {
        return Admin.fetchAll({ withRelated: ['role'] }).then(function (models) {
            return models.serialize();
        });
    };
    AdminM.prototype.createAdmin = function (input) {
        if (input) {
            return new Admin(input).save().then(function (model) {
                return model;
            });
        }
        return false;
    };
    AdminM.prototype.deleteAdmin = function (id) {
        if (id) {
            return Admin.where('id', id).destroy();
        }
        return false;
    };
    AdminM.prototype.updateAdmin = function (id, input) {
        if (id) {
            return Admin.where('id', id).set(input).save(null, { method: 'update' });
        }
        return false;
    };
    return AdminM;
}());
exports.AdminModel = new AdminM();
