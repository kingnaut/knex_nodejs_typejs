"use strict";
exports.__esModule = true;
var connectors_1 = require("../connectors");
var promise = require("bluebird");
var Member = /** @class */ (function () {
    function Member() {
    }
    Member.prototype.getAllMembers = function () {
        return connectors_1.knex('members').select();
    };
    Member.prototype.createMember = function (input) {
        if (input) {
            return connectors_1.knex('members').insert(input).then(function (result) {
                return connectors_1.knex('members').where('id', result[0]).first();
            });
        }
        return false;
    };
    Member.prototype.deleteMember = function (id) {
        if (id) {
            return connectors_1.knex('members').where('id', id).del();
        }
        return false;
    };
    Member.prototype.updateMember = function (id, input) {
        if (id) {
            return connectors_1.knex('members').where('id', id).update(input);
        }
        return false;
    };
    Member.prototype.insertMembers = function (members) {
        return connectors_1.knex.transaction(function (trans) {
            promise.map(members, function (member) {
                return connectors_1.knex.insert(member).into('members').transacting(trans);
            })
                .then(trans.commit)["catch"](trans.rollback);
        })
            .then(function (inserts) {
            console.log(inserts.length);
        })["catch"](function (error) {
            console.log("rollback db");
            console.error(error);
            return false;
        });
    };
    return Member;
}());
exports.MemberModel = new Member();
