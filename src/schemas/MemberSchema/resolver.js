"use strict";
exports.__esModule = true;
var member_1 = require("../../models/member");
exports.resolver = {
    Query: {
        getAllMembers: function () { return member_1.MemberModel.getAllMembers(); }
    },
    Mutation: {
        createMember: function (root, _a) {
            var input = _a.input;
            return member_1.MemberModel.createMember(input);
        },
        deleteMember: function (root, _a) {
            var id = _a.id;
            return member_1.MemberModel.deleteMember(id);
        },
        updateMember: function (root, _a) {
            var id = _a.id, input = _a.input;
            if (id) {
                return member_1.MemberModel.updateMember(id, input);
            }
            return false;
        },
        insertMembers: function (root, _a) {
            var input = _a.input;
            return member_1.MemberModel.insertMembers(input);
        }
    }
};
