"use strict";
exports.__esModule = true;
exports.typeDef = "\n    extend type Query {\n        getAllMembers: [Member]\n    }\n\n    extend type Mutation {\n        createMember(input: MemberInput): Member\n        deleteMember(id: ID!): Boolean\n        updateMember(id: ID!, input: MemberInput): Boolean\n        insertMembers(input: [MemberInput]): Boolean\n    }\n\n    input MemberInput {\n        login_id: String,\n        nickname: String,\n    }\n\n    type Member {\n        id: ID,\n        login_id: String,\n        nickname: String,\n    }\n";
