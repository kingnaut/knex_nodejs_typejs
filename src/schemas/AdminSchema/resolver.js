"use strict";
exports.__esModule = true;
var admin_1 = require("../../models/admin");
exports.resolver = {
    Query: {
        getAllAdmins: function () { return admin_1.AdminModel.getAllAdmins(); }
    },
    Mutation: {
        createAdmin: function (root, _a) {
            var input = _a.input;
            return admin_1.AdminModel.createAdmin(input);
        },
        deleteAdmin: function (root, _a) {
            var id = _a.id;
            return admin_1.AdminModel.deleteAdmin(id);
        },
        updateAdmin: function (root, _a) {
            var id = _a.id, input = _a.input;
            if (id) {
                return admin_1.AdminModel.updateAdmin(id, input);
            }
            return false;
        }
    }
};
