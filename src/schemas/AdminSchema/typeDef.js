"use strict";
exports.__esModule = true;
exports.typeDef = "\n    extend type Query {\n        getAllAdmins: [Admin]\n    }\n\n    extend type Mutation {\n        createAdmin(input: AdminInput): Admin\n        deleteAdmin(id: ID!): Boolean\n        updateAdmin(id: ID!, input: AdminInput): Boolean\n    }\n\n    input AdminInput {\n        mail_address: String,\n        password: String,\n        user_name: String,\n        role_id: Int\n    }\n\n    type Admin {\n        id: ID,\n        mail_address: String,\n        password: String,\n        user_name: String,\n        role: Role\n    }\n";
