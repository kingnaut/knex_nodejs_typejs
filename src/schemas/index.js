"use strict";
exports.__esModule = true;
var lodash_1 = require("lodash");
var MemberSchema_1 = require("./MemberSchema");
var AdminSchema_1 = require("./AdminSchema");
var RoleSchema_1 = require("./RoleSchema");
var DefaultSchema = "\n    type Query {\n        _empty: String\n    }\n\n    type Mutation {\n        _empty: String\n    }\n";
exports.typeDefs = [DefaultSchema, MemberSchema_1.typeDef, RoleSchema_1.typeDef, AdminSchema_1.typeDef];
exports.resolvers = lodash_1.merge(MemberSchema_1.resolver, AdminSchema_1.resolver);
