"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const admin_1 = require("../../models/admin");
exports.resolver = {
    Query: {
        getAllAdmins: () => admin_1.AdminModel.getAllAdmins(),
    },
    Mutation: {
        createAdmin(root, { input }) {
            return admin_1.AdminModel.createAdmin(input);
        },
        deleteAdmin(root, { id }) {
            return admin_1.AdminModel.deleteAdmin(id);
        },
        updateAdmin(root, { id, input }) {
            if (id) {
                return admin_1.AdminModel.updateAdmin(id, input);
            }
            return false;
        }
    }
};
//# sourceMappingURL=resolver.js.map