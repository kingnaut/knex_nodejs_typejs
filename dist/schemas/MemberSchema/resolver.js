"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const member_1 = require("../../models/member");
exports.resolver = {
    Query: {
        getAllMembers: () => member_1.MemberModel.getAllMembers(),
    },
    Mutation: {
        createMember(root, { input }) {
            return member_1.MemberModel.createMember(input);
        },
        deleteMember(root, { id }) {
            return member_1.MemberModel.deleteMember(id);
        },
        updateMember(root, { id, input }) {
            if (id) {
                return member_1.MemberModel.updateMember(id, input);
            }
            return false;
        },
        insertMembers(root, { input }) {
            return member_1.MemberModel.insertMembers(input);
        }
    }
};
//# sourceMappingURL=resolver.js.map